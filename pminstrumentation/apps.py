from django.apps import AppConfig


class PminstrumentationConfig(AppConfig):
    name = 'pminstrumentation'
