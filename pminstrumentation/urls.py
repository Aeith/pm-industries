from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
    path('about', views.wip, name='about'),
    path('contact', views.wip, name='contact'),
    path('legals', views.wip, name='legal')
]
