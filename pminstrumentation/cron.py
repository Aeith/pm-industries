from django.core.mail import send_mail
from django.utils.timezone import datetime

from notifications.models import Notification

def send(request):
    today = datetime.today().date()
    notifications = Notification.objects.all().filter(automatized=True)

    for notification in notifications:
        # Get last notification date
        date = notification.date.date()
        day = date.day
        month = date.month
        year = date.year

        # Get recurrence delay (format 0D0M0Y)
        recurrence = notification.recurrence
        daySplit = recurrence.split("D")
        day_delay = daySplit[0]
        monthSplit = daySplit[1].split("M")
        month_delay = monthSplit[0]
        year_delay = monthSplit[1].split("Y")[0]

        # Send notification when today's date passed delay
        if (day + day_delay) >= today.day :
            if (month + month_delay) >= today.month :
                if (year + year_delay) >= today.year :
                    send_email(
                    'Notification : ' + notification.type.type,
                    notification.message,
                    'from@example.com',                                         #sending email
                    notification.to,
                    fail_silently=False,)

                    notification.date = date.today()
                    notification.save()
    pass
