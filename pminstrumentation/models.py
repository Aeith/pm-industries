from django.db import models
from sites.models import Site
from django.contrib.auth.models import User

class PortalUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    site = models.ForeignKey(Site, on_delete=models.CASCADE)
