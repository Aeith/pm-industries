from django import forms

from .models import PortalUser

class LoginForm(forms.ModelForm):
    mail = forms.CharField(widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder':'Adresse mail'
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class':'form-control',
        'placeholder':'Mot de passe'
    }))
    class Meta:
        model = PortalUser
        fields = [
            "mail",
            "password"
        ]
