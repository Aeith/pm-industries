from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout

from .forms import LoginForm


def index(request):
    return render(request,'pmi/index.html',{})

def login(request):
    if request.method == 'POST' :
        form = LoginForm(request.POST)
        if form.is_valid():
            #check Connexion
            user = authenticate(request,username=request.POST.get('mail'), password=request.POST.get('password'))
            if user is not None:
                if user.is_active:
                    auth_login(request,user)
                    if request.GET.get('next') is not None:
                        return HttpResponseRedirect(request.GET.get('next'))
                    else :
                        return HttpResponseRedirect('/about')
            else :
                return HttpResponseRedirect('/login')
    else :
        form = LoginForm()
        return render(request,'pminstrumentation/index.html',{'form': form})

def logout(request):
    if request.user is not None:
        auth_logout(request)
    if request.GET.get('next') is not None:
        return HttpResponseRedirect(request.GET.get('next'))
    return HttpResponseRedirect('/about')

def wip(request):
    return render(request, "wip.html", {})
