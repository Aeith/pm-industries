from django.db import models
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.core.validators import MaxValueValidator, MinValueValidator

class Customer(models.Model):
    name = models.CharField(max_length=500)
    label = models.CharField(max_length=500)
    siren = models.IntegerField(validators=[MinValueValidator(100000000),MaxValueValidator(999999999)]) #9 digits

    def get_absolute_url(self):
        return reverse("customers:customer-detail", kwargs={"id":self.id})
