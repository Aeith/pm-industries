from django.urls import path

from .views import (
    CustomerDetailView,
    CustomerCreateView,
    CustomerUpdateView,
    CustomerDeleteView,
    export_customers_csv,
    import_customers_csv,
    customers_grid
)

app_name = "customers"
urlpatterns = [
    path('', customers_grid, name='customer-index'),
    path('<int:id>/', CustomerDetailView.as_view(), name='customer-detail'),
    path('create/', CustomerCreateView.as_view(), name='customer-creation'),
    path('<int:id>/update/', CustomerUpdateView.as_view(), name='customer-update'),
    path('<int:id>/delete/', CustomerDeleteView.as_view(), name='customer-delete'),
    path('export', export_customers_csv, name='customer-export-csv'),
    path('import', import_customers_csv, name='customer-import-csv'),
]
