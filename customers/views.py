from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404
from django.urls import reverse
from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView
from django.contrib.auth.decorators import login_required, permission_required

from .models import Customer
from .forms import CustomerModelForm

import csv, os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

@login_required
@permission_required("customers_write","customers_all")
def import_customers_csv(request):
    if request.method == 'POST' and request.FILES:                              #avoid GET and empty file sending
        file = request.FILES['file'].read().decode("utf-8")             #get the file and open it to read with UTF-8 compatibility
        rows = file.split("\n")                                                 #get rows (separator : \n)
        for row in rows:                                                        #loop through rows
            if row != '':                                                       #avoid last empty row
                col = row.split(",")                                            #get columns (delimiter : ,)
                if col[0] != 'name':                                            #skip header
                    _, created = Customer.objects.get_or_create(                #check existence in database to avoid duplicate
                        name        = col[0],
                        label       = col[1],
                        siren       = col[2],
                    )

    return redirect("../customers")

@login_required
@permission_required("customers_read","customers_all")
def export_customers_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="customers.csv"'

    writer = csv.writer(response)
    writer.writerow(['name','label','siren'])

    customers = Customer.objects.all().values_list('name','label','siren')
    for customer in customers:
        writer.writerow(customer)

    return response

@login_required
@permission_required("customers_read","customers_all")
def customers_grid(request, *args, **kwargs):
    template_name = 'customers/customer_grid.html'

    if request.POST:
        move = request.POST.get("pagination")
        if move is not None and move != "":
            active_page = int(request.POST.get("active_page"))

            if move == "&laquo;" or move == "«":
                active_page -= 1
            elif move == "&raquo;" or move == "»":
                active_page += 1
            else:
                active_page = int(move)
    else:
        active_page = 1 #dynamic

    queryset = Customer.objects.all().order_by('id')[10*(active_page-1):10*active_page]      #range of 10 items
    max_page = int(Customer.objects.all().count()/10)+1                                         #total amount of pages = items/10 +1
    display_pages = [1,2,3,4,5]                                                             #max 5 pages at once
    if active_page >= 4:                                                                    #once 4 is passed
        display_pages = [x+(active_page-3) for x in display_pages]                          #move page count to follow active_page value

    context = {
        "object_list":queryset,
        "active_page":active_page,
        "max_page":max_page,
        "display":display_pages,
        "count":Customer.objects.all().count()
    }

    return render(request, template_name, context=context)

class CustomerDetailView(DetailView):
    template_name = 'customers/customer_detail.html'

    def get_object(self):
    	id_ = self.kwargs.get("id")
    	return get_object_or_404(Customer, id=id_)

class CustomerCreateView(CreateView):
    template_name = 'customers/customer_create.html'
    success_url = "../"

    form_class = CustomerModelForm
    queryset = Customer.objects.all()

    def get_success_url(self, **kwargs):
        return self.success_url

    def form_valid(self, form):
        return super().form_valid(form)

class CustomerUpdateView(UpdateView):
    template_name = 'customers/customer_update.html'
    form_class = CustomerModelForm
    queryset = Customer.objects.all()

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Customer, id=id_)

    def form_valid(self, form):
        return super().form_valid(form)

class CustomerDeleteView(DeleteView):
    template_name = 'customers/customer_delete.html'

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Customer, id=id_)

    def get_success_url(self):
        return reverse("customers:customer-index")
