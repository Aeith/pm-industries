from django.db import models
from django.urls import reverse
from django.http import HttpResponseRedirect

class Category(models.Model):
    name = models.CharField(max_length=500)

    def get_absolute_url(self):
        return reverse("categories:category-detail", kwargs={"id":self.id})
