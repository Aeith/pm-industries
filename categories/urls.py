from django.urls import path

from .views import (
    CategoryDetailView,
    CategoryCreateView,
    CategoryUpdateView,
    CategoryDeleteView,
    export_categories_csv,
    import_categories_csv,
    categories_grid
)

app_name = "categories"
urlpatterns = [
    path('', categories_grid, name='category-grid'),
    path('<int:id>/', CategoryDetailView.as_view(), name='category-detail'),
    path('create/', CategoryCreateView.as_view(), name='category-creation'),
    path('<int:id>/update/', CategoryUpdateView.as_view(), name='category-update'),
    path('<int:id>/delete/', CategoryDeleteView.as_view(), name='category-delete'),
    path('export', export_categories_csv, name='category-export-csv'),
    path('import', import_categories_csv, name='category-import-csv'),
]
