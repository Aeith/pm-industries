from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404
from django.urls import reverse
from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView
from django.contrib.auth.decorators import login_required, permission_required

from .models import Category
from .forms import CategoryModelForm

import csv, os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

@login_required
@permission_required("categories_write","categories_all")
def import_categories_csv(request):
    if request.method == 'POST' and request.FILES:                              #avoid GET and empty file sending
        file = request.FILES['file'].read().decode("utf-8")                     #get the file and open it to read with UTF-8 compatibility
        rows = file.split("\n")                                                 #get rows (separator : \n)
        for row in rows:                                                        #loop through rows
            if row != '':                                                       #avoid last empty row
                col = row.split(",")                                            #get columns (delimiter : ,)
                if col[0] != 'name':                                            #skip header
                    _, created = Category.objects.get_or_create(                #check existence in database to avoid duplicate
                        name        = col[0],
                    )

    return redirect("../categories")

@login_required
@permission_required("categories_read","categories_all")
def export_categories_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="categories.csv"'

    writer = csv.writer(response)
    writer.writerow(['name'])

    categories = Category.objects.all().values_list('name')
    for category in categories:
        writer.writerow(category)

    return response

@login_required
@permission_required("categories_read","categories_all")
def categories_grid(request, *args, **kwargs):
    template_name = 'categories/category_grid.html'

    if request.POST:
        move = request.POST.get("pagination")
        if move is not None and move != "":
            active_page = int(request.POST.get("active_page"))

            if move == "&laquo;" or move == "«":
                active_page -= 1
            elif move == "&raquo;" or move == "»":
                active_page += 1
            else:
                active_page = int(move)
    else:
        active_page = 1 #dynamic

    queryset = Category.objects.all().order_by('id')[10*(active_page-1):10*active_page]      #range of 10 items
    max_page = int(Category.objects.all().count()/10)+1                                         #total amount of pages = items/10 +1
    display_pages = [1,2,3,4,5]                                                             #max 5 pages at once
    if active_page >= 4:                                                                    #once 4 is passed
        display_pages = [x+(active_page-3) for x in display_pages]                          #move page count to follow active_page value

    context = {
        "object_list":queryset,
        "active_page":active_page,
        "max_page":max_page,
        "display":display_pages,
        "count":Category.objects.all().count()
    }

    return render(request, template_name, context=context)

class CategoryDetailView(DetailView):
    template_name = 'categories/category_detail.html'

    def get_object(self):
    	id_ = self.kwargs.get("id")
    	return get_object_or_404(Category, id=id_)

class CategoryCreateView(CreateView):
    template_name = 'categories/category_create.html'
    success_url = "../"

    form_class = CategoryModelForm
    queryset = Category.objects.all()

    def get_success_url(self, **kwargs):
        return self.success_url

    def form_valid(self, form):
        return super().form_valid(form)

class CategoryUpdateView(UpdateView):
    template_name = 'categories/category_update.html'
    form_class = CategoryModelForm
    queryset = Category.objects.all()

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Category, id=id_)

    def form_valid(self, form):
        return super().form_valid(form)

class CategoryDeleteView(DeleteView):
    template_name = 'categories/category_delete.html'

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Category, id=id_)

    def get_success_url(self):
        return reverse("categories:category-grid")
