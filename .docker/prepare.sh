#!/bin/bash

FILE=.env
PROJECT_NAME=pm-instrumentation

declare -A vars

touch $FILE

vars[PROJECT_NAME]="$PROJECT_NAME"
vars[PROJECT_PATH]="$(realpath $PWD)"
vars[VOLUME_OPTIONS]="rw"
vars[ENVIRONMENT]="DEV"
vars[DJANGO_SECRET_KEY]="dont-tell-eve"
vars[DJANGO_DEBUG]="yes"
vars[DB_NAME]="pminstrumentation"
vars[DB_USER]="pmi"
vars[DB_PWD]="xxxx"
vars[IP]="127.0.0.1"
vars[PORT]="8000"
vars[HOSTS]=

for k in "${!vars[@]}"; do
        grep=$(grep "^$k=" "$FILE" || true)
        [ "$grep" != ""  ] && continue
        echo "$k=${vars[$k]}" >> "$FILE"
done

DJANGO_ENVFILE=".docker/django/env"
django_variables=($(cat $DJANGO_ENVFILE | awk -F= '{print $1}' | tr '\n' ' '))
for i in "${django_variables[@]}"; do
  grep=$(grep "$i" "$FILE")
  [ "$grep" != "" ] && continue
  line=$(grep $i $DJANGO_ENVFILE)
  [ "$line" == "" ] && continue
  echo $line >> $FILE
done
