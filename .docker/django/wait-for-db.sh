#!/bin/sh

set -e

cmd="$@"

until mysql -h $DB_HOST -u $DB_USER -p$DB_PWD -e"quit"; do
  >&2 echo "Mysql is unavailable - sleeping"
  sleep 5
done

>&2 echo "Mysql is up - executing command"
exec $cmd
