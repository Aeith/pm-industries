PREPARE=.docker/prepare.sh

-include: .env

.env: $(PREPARE)
	$(PREPARE)

.PHONY: docker-prepare
docker-prepare:
	$(PREPARE)

.PHONY: docker-up
docker-up: docker-prepare
	docker-compose up -d --remove-orphans

.PHONY: docker-build
docker-build:
	docker-compose build

.PHONY: docker-down
docker-down:
	docker-compose down -v

.PHONY: up
up: docker-up

.PHONY: down
down: docker-down

.PHONY: init
init: .env docker-build docker-up test

.PHONY: test
test:
	./.docker/django/test
