from django.urls import path

from . import views
# from .views import (
#     CategoryDetailView,
#     CategoryListView,
#     CategoryCreateView,
#     CategoryUpdateView,
#     CategoryDeleteView,
#     ProductDetailView,
#     ProductCreateView,
#     ProductUpdateView,
#     ProductDeleteView,
#     export_products_csv,
#     import_products_csv,
#     export_categories_csv,
#     import_categories_csv,
#     products_grid
# )

app_name = "planning"
urlpatterns = [
    path('', views.index, name='planning-index'),
    # path('<int:id>/', ProductDetailView.as_view(), name='product-detail'),
    # path('create/', ProductCreateView.as_view(), name='product-creation'),
    # path('<int:id>/update/', ProductUpdateView.as_view(), name='product-update'),
    # path('<int:id>/delete/', ProductDeleteView.as_view(), name='product-delete'),
    # path('categories/', CategoryListView.as_view(), name='category-list'),
    # #path('categories/grid/', categories_grid, name="grid-test"),
    # path('categories/<int:id>/', CategoryDetailView.as_view(), name='category-detail'),
    # path('categories/create/', CategoryCreateView.as_view(), name='category-creation'),
    # path('categories/<int:id>/update/', CategoryUpdateView.as_view(), name='category-update'),
    # path('categories/<int:id>/delete/', CategoryDeleteView.as_view(), name='category-delete'),
    # path('export-products', export_products_csv, name='product-export-csv'),
    # path('export-categories', export_categories_csv, name='categories-export-csv'),
    # path('import-products', import_products_csv, name='product-import-csv'),
    # path('import-categories', import_categories_csv, name='categories-import-csv'),
]
