from django.db import models
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.utils.timezone import datetime

from customers.models import Customer
from products.models import Product

class Type(models.Model):
    type = models.CharField(max_length=50)

class State(models.Model):
    state = models.CharField(max_length=20)

class Notification(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    type = models.ForeignKey(Type, on_delete=models.CASCADE)
    date = models.DateTimeField(default=datetime.today())
    automatized = models.BooleanField(default=False)
    recurrence = models.CharField(max_length=10, null=True,blank=True)
    message = models.TextField(default="")

    def get_absolute_url(self):
        return reverse("notifications:notification-detail", kwargs={"id":self.id})
