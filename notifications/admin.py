from django.contrib import admin

from .models import Notification, Type, State

admin.site.register(Type)
admin.site.register(State)
admin.site.register(Notification)
