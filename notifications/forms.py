from django import forms

from .models import Notification

class NotificationModelForm(forms.ModelForm):
    class Meta:
            model = Notification
            fields = [
                "customer",
                "product",
                "state",
                "type",
                "date",
                "automatized",
                "recurrence",
            ]
