from django.urls import path

from .views import (
    NotificationDetailView,
    NotificationCreateView,
    NotificationUpdateView,
    NotificationDeleteView,
    export_notifications_csv,
    import_notifications_csv,
    notifications_grid
)

app_name = "notifications"
urlpatterns = [
    path('', notifications_grid, name='notification-grid'),
    path('<int:id>/', NotificationDetailView.as_view(), name='notification-detail'),
    path('create/', NotificationCreateView.as_view(), name='notification-creation'),
    path('<int:id>/update/', NotificationUpdateView.as_view(), name='notification-update'),
    path('<int:id>/delete/', NotificationDeleteView.as_view(), name='notification-delete'),
    path('export', export_notifications_csv, name='notification-export-csv'),
    path('import', import_notifications_csv, name='notification-import-csv'),
]
