from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404
from django.urls import reverse
from django.views.generic import CreateView, DetailView, UpdateView, DeleteView

from categories.models import Category
from customers.models import Customer
from products.models import Product
from .models import Notification, Type, State
from .forms import NotificationModelForm

import csv, os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def import_notifications_csv(request):
    if request.method == 'POST' and request.FILES:
        file = request.FILES['file'].read().decode("utf-8")
        rows = file.split("\n")
        for row in rows:
            if row != '':
                col = row.split(",")
                if col[0] != 'date':    #skip header
                    if(col[2] != ""):
                        _cus, cus = Customer.objects.get_or_create(
                            id=col[2],
                            defaults={
                            "name":"Default customer name",
                            "siren":"123456789"}
                        )
                    _cat, cat = Category.objects.get_or_create(
                        id=1,
                        defaults={
                        "name":"Default category name"}
                    )

                    if isinstance(cat, Category):
                        category = cat
                    else:
                        category = _cat

                    _pro, pro = Product.objects.get_or_create(
                        id=col[3],
                        defaults={
                        "name":"Default product name",
                        "category":category}
                    )
                    _typ, typ = Type.objects.get_or_create(
                        id=col[1],
                        defaults={"type":"Default type"}
                    )
                    _sta, sta = State.objects.get_or_create(
                        id=col[4],
                        defaults={"state":"Default state"}
                    )

                    if(col[2] != ""):
                        if isinstance(cus, Customer):
                            customer = cus
                        else:
                            customer = _cus
                    else:
                        customer = None

                    if isinstance(pro, Product):
                        product = pro
                    else:
                        product = _pro

                    if isinstance(typ, Type):
                        type = typ
                    else:
                        type = _typ

                    if isinstance(sta, State):
                        state = sta
                    else:
                        state = _sta

                    _, created = Notification.objects.get_or_create(
                        customer    = customer,
                        product     = product,
                        state       = state,
                        type        = type,
                        date        = col[0]
                    )

    return redirect(".")

def export_notifications_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="notifications.csv"'

    writer = csv.writer(response)
    writer.writerow(['date','type', 'customer', 'product', 'state'])

    notifications = Notification.objects.all().values_list('date','type', 'customer', 'product', 'state')
    for notification in notifications:
        writer.writerow(notification)

    return response

def notifications_grid(request, *args, **kwargs):
    template_name = 'notifications/notification_grid.html'
    if request.POST:
        move = request.POST.get("pagination")
        if move is not None and move != "":
            active_page = int(request.POST.get("active_page"))

            if move == "&laquo;" or move == "«":
                active_page -= 1
            elif move == "&raquo;" or move == "»":
                active_page += 1
            else:
                active_page = int(move)
    else:
        active_page = 1 #dynamic

    queryset = Notification.objects.all().order_by('id')[10*(active_page-1):10*active_page]      #range of 10 items
    max_page = int(Notification.objects.all().count()/10)+1                                         #total amount of pages = items/10 +1
    display_pages = [1,2,3,4,5]                                                             #max 5 pages at once
    if active_page >= 4:                                                                    #once 4 is passed
        display_pages = [x+(active_page-3) for x in display_pages]                          #move page count to follow active_page value

    context = {
        "object_list":queryset,
        "active_page":active_page,
        "max_page":max_page,
        "display":display_pages,
        "count":Notification.objects.all().count()
    }

    return render(request, template_name, context=context)

class NotificationDetailView(DetailView):
    template_name = 'notifications/notification_detail.html'

    def get_object(self):
    	id_ = self.kwargs.get("id")
    	return get_object_or_404(Notification, id=id_)

class NotificationCreateView(CreateView):
    template_name = 'notifications/notification_create.html'
    success_url = "../"

    form_class = NotificationModelForm
    queryset = Notification.objects.all()

    def get_success_url(self, **kwargs):
        return self.success_url

    def form_valid(self, form):
        return super().form_valid(form)

class NotificationUpdateView(UpdateView):
    template_name = 'notifications/notification_update.html'
    form_class = NotificationModelForm
    queryset = Notification.objects.all()

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Notification, id=id_)

    def form_valid(self, form):
        return super().form_valid(form)

class NotificationDeleteView(DeleteView):
    template_name = 'notifications/notification_delete.html'

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Notification, id=id_)

    def get_success_url(self):
        return reverse("notifications:notification-grid")
