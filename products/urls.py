from django.urls import path

from .views import (
    ProductDetailView,
    ProductCreateView,
    ProductUpdateView,
    ProductDeleteView,
    export_products_csv,
    import_products_csv,
    products_grid
)

app_name = "products"
urlpatterns = [
    path('', products_grid, name='product-grid'),
    path('<int:id>/', ProductDetailView.as_view(), name='product-detail'),
    path('create/', ProductCreateView.as_view(), name='product-creation'),
    path('<int:id>/update/', ProductUpdateView.as_view(), name='product-update'),
    path('<int:id>/delete/', ProductDeleteView.as_view(), name='product-delete'),
    path('export', export_products_csv, name='product-export-csv'),
    path('import', import_products_csv, name='product-import-csv'),
]
