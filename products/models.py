from django.db import models
from django.urls import reverse
from django.http import HttpResponseRedirect

from categories.models import Category

class Product(models.Model):
    name = models.CharField(max_length=500)
    description = models.TextField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    reference = models.CharField(max_length=100)

    def get_absolute_url(self):
        return reverse("products:product-detail", kwargs={"id":self.id})
