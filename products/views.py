from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404
from django.urls import reverse
from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView
from django.contrib.auth.decorators import login_required, permission_required

from categories.models import Category
from .models import Product
from .forms import ProductModelForm

import csv, os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

@login_required
@permission_required("products_write","products_all")
def import_products_csv(request):
    if request.method == 'POST' and request.FILES:
        file = request.FILES['file'].read().decode("utf-8")
        rows = file.split("\n")
        for row in rows:
            if row != '':
                col = row.split(",")
                if col[0] != 'category':    #skip header
                    _, cat = Category.objects.get_or_create(
                        id=col[0],
                        defaults={"name":"Default category name"}
                    )

                    if isinstance(cat, Category):
                        category = cat
                    else:
                        category = _

                    _, created = Product.objects.get_or_create(
                        category    = category,
                        name        = col[1],
                        description = col[2],
                        reference   = col[3]
                    )

    return redirect(".")

@login_required
def export_products_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="products.csv"'

    writer = csv.writer(response)
    writer.writerow(['category','name', 'description', 'reference'])

    products = Product.objects.all().values_list('category','name', 'description', 'reference')
    for product in products:
        writer.writerow(product)

    return response

@login_required
def products_grid(request, *args, **kwargs):
    template_name = 'products/product_grid.html'

    if request.POST:
        move = request.POST.get("pagination")
        if move is not None and move != "":
            active_page = int(request.POST.get("active_page"))

            if move == "&laquo;" or move == "«":
                active_page -= 1
            elif move == "&raquo;" or move == "»":
                active_page += 1
            else:
                active_page = int(move)
    else:
        active_page = 1 #dynamic

    queryset = Product.objects.all().order_by('id')[10*(active_page-1):10*active_page]      #range of 10 items
    max_page = int(Product.objects.all().count()/10)+1                                         #total amount of pages = items/10 +1
    display_pages = [1,2,3,4,5]                                                             #max 5 pages at once
    if active_page >= 4:                                                                    #once 4 is passed
        display_pages = [x+(active_page-3) for x in display_pages]                          #move page count to follow active_page value

    context = {
        "object_list":queryset,
        "active_page":active_page,
        "max_page":max_page,
        "display":display_pages,
        "count":Product.objects.all().count()
    }

    return render(request, template_name, context=context)

class ProductDetailView(DetailView):
    template_name = 'products/product_detail.html'

    @login_required
    def get_object(self):
    	id_ = self.kwargs.get("id")
    	return get_object_or_404(Product, id=id_)

class ProductCreateView(CreateView):
    template_name = 'products/product_create.html'
    success_url = "../"

    form_class = ProductModelForm
    queryset = Product.objects.all()
    @login_required
    def get_success_url(self, form):
        return success_url
    @login_required
    def form_valid(self, form):
        return super().form_valid(form)

class ProductUpdateView(UpdateView):
    template_name = 'products/product_update.html'
    form_class = ProductModelForm
    queryset = Product.objects.all()
    @login_required
    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Product, id=id_)
    @login_required
    def form_valid(self, form):
        return super().form_valid(form)

class ProductDeleteView(DeleteView):
    template_name = 'products/product_delete.html'

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Product, id=id_)

    def get_success_url(self):
        return reverse("products:product-grid")
