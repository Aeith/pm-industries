# PM Instrumentation

## Description

Outils de CRM pour le support et la maintenance du parc de capteur des clients de l'entreprise PM INSTRUMENTATION 

## Installation de Docker et Docker Compose
Suivre les installations sur la documentation officielle :
- [docker](https://docs.docker.com/v17.12/install/)

- [docker-compose](https://docs.docker.com/compose/install/)

Sur linux, pour éviter de donner les droits super utilisateur à nos conteneurs, ajouter l'utilisateur courant dans le groupe docker :
```
sudo usermod -a -G docker <utilisateur>
```

## Initialisation du projet

Cloner le dépôt et lancer le make
```
git clone git@gitlab.com:Aeith/pm-industries.git
cd pm-industries
make init
```
Le make init est lancé une seule fois et se charge d'initialiser et de lancer les conteneurs

## Paramétrage des allowed hosts

Dans le fichier .env, completer la clé HOSTS avec les hostnames et adresses IP que vous souhaiter utiliser séparés par des virgules

```
HOSTS=localhost,127.0.0.1
```

Avec cette configuration, le projet sera accessible sur le navigateur aux adresses :

* http://localhost:8000
* http://127.0.0.1:8000

## Démarrage du projet

Pour lancer le projet :
```
make up
```

## Arrêter les services
Pour stopper les conteneurs :
```
make down
```

## Executer des commandes à l'interieur du container Django

```
./manage {commande django}

exemple:

./manage createsuperuser
```