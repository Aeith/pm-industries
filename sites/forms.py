from django import forms

from .models import Site

class SiteModelForm(forms.ModelForm):
    class Meta:
            model = Site
            fields = [
                "customer",
                "name",
                "country",
                "state",
                "city",
                "postal",
                "address"
            ]
