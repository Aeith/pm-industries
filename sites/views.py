from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404
from django.urls import reverse
from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView
from django.contrib.auth.decorators import login_required, permission_required
from customers.models import Customer
from .models import Site
from .forms import SiteModelForm

import csv, os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

@permission_required("sites_write","sites_all")
def import_sites_csv(request):
    if request.method == 'POST' and request.FILES:                              #avoid GET and empty file sending
        file = request.FILES['file'].read().decode("utf-8")           #get the file and open it to read with UTF-8 compatibility
        rows = file.split("\n")                                                 #get rows (separator : \n)
        for row in rows:                                                        #loop through rows
            if row != '':                                                       #avoid last empty row
                col = row.split(",")                                            #get columns (delimiter : ,)
                if col[0] != 'customer':                                            #skip header
                    _, cus = Customer.objects.get_or_create(
                        id=col[0],
                        defaults={"name":"Default customer","label":"Default customer label","siren":"999999999"}
                    )

                    if isinstance(cus, Customer):
                        customer = cus
                    else:
                        customer = _

                    _, created = Site.objects.get_or_create(                #check existence in database to avoid duplicate
                        customer    = customer,
                        name        = col[1],
                        country     = col[2],
                        state       = col[3],
                        city        = col[4],
                        postal      = col[5],
                        address     = col[6],
                    )

    return redirect("../sites")

@permission_required("sites.view_site")
def export_sites_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="sites.csv"'

    writer = csv.writer(response)
    writer.writerow(['customer','name','country','state','city','postal','address'])

    sites = Site.objects.all().values_list('customer','name','country','state','city','postal','address')
    for site in sites:
        writer.writerow(site)

    return response

@permission_required("sites_read")
def sites_grid(request, *args, **kwargs):
    template_name = 'sites/site_grid.html'

    if request.POST:
        move = request.POST.get("pagination")
        if move is not None and move != "":
            active_page = int(request.POST.get("active_page"))

            if move == "&laquo;" or move == "«":
                active_page -= 1
            elif move == "&raquo;" or move == "»":
                active_page += 1
            else:
                active_page = int(move)
    else:
        active_page = 1 #dynamic

    queryset = Site.objects.all().order_by('id')[10*(active_page-1):10*active_page]      #range of 10 items
    max_page = int(Site.objects.all().count()/10)+1                                         #total amount of pages = items/10 +1
    display_pages = [1,2,3,4,5]                                                             #max 5 pages at once
    if active_page >= 4:                                                                    #once 4 is passed
        display_pages = [x+(active_page-3) for x in display_pages]                          #move page count to follow active_page value

    context = {
        "object_list":queryset,
        "active_page":active_page,
        "max_page":max_page,
        "display":display_pages,
        "count":Site.objects.all().count()
    }

    return render(request, template_name, context=context)

class SiteDetailView(DetailView):
    template_name = 'sites/site_detail.html'

    def get_object(self):
    	id_ = self.kwargs.get("id")
    	return get_object_or_404(Site, id=id_)

class SiteCreateView(CreateView):
    template_name = 'sites/site_create.html'
    success_url = "../"

    form_class = SiteModelForm
    queryset = Site.objects.all()

    def get_success_url(self, **kwargs):
        return self.success_url

    def form_valid(self, form):
        return super().form_valid(form)

class SiteUpdateView(UpdateView):
    template_name = 'sites/site_update.html'
    form_class = SiteModelForm
    queryset = Site.objects.all()

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Site, id=id_)

    def form_valid(self, form):
        return super().form_valid(form)

class SiteDeleteView(DeleteView):
    template_name = 'sites/site_delete.html'

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Site, id=id_)

    def get_success_url(self):
        return reverse("sites:site-grid")
