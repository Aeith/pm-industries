from django.db import models
from django.urls import reverse
from django.http import HttpResponseRedirect

from customers.models import Customer

class Site(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    name = models.CharField(max_length=500)
    country = models.CharField(max_length=500)
    state = models.CharField(max_length=500)
    city = models.CharField(max_length=500)
    postal = models.CharField(max_length=500)
    address = models.CharField(max_length=500)

    def get_absolute_url(self):
        return reverse("sites:site-detail", kwargs={"id":self.id})
