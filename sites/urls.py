from django.urls import path

from .views import (
    SiteDetailView,
    SiteCreateView,
    SiteUpdateView,
    SiteDeleteView,
    export_sites_csv,
    import_sites_csv,
    sites_grid
)

app_name = "sites"
urlpatterns = [
    path('', sites_grid, name='site-grid'),
    path('<int:id>/', SiteDetailView.as_view(), name='site-detail'),
    path('create/', SiteCreateView.as_view(), name='site-creation'),
    path('<int:id>/update/', SiteUpdateView.as_view(), name='site-update'),
    path('<int:id>/delete/', SiteDeleteView.as_view(), name='site-delete'),
    path('export', export_sites_csv, name='site-export-csv'),
    path('import', import_sites_csv, name='site-import-csv'),
]
